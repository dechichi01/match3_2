﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public static class Randomness {
    //Returns a shuffled array (can be used to avoid repetition)
    public static T[] ShuffleArrayAlloc<T> (T[] array, int seed) {
        return ShuffleArrayAlloc (array, new System.Random (seed));
    }

    public static T[] ShuffleArrayAlloc<T> (T[] array, System.Random prng) {
        T[] newArray = new T[array.Length];
        Array.Copy (array, newArray, array.Length);

        ShuffleArray (newArray, prng);
        return newArray;
    }

    public static T[] ShuffleArray<T> (T[] array, int seed) {
        return ShuffleArray (array, new System.Random (seed));
    }

    public static T[] ShuffleArray<T> (T[] array, System.Random prng) {
        for (int i = 0; i < array.Length; i++) {
            int randomIndex = prng.Next (i, array.Length);

            //Swaps current item with a random item in the array
            T tempItem = array[randomIndex];
            array[randomIndex] = array[i];
            array[i] = tempItem;
        }

        return array;
    }

    public static T GetRandom<T> (this List<T> collection) {
        if (collection.Count == 0) return default (T);
        return collection[Random.Range (0, collection.Count)];
    }

    //Get a random value with probability (see ProbabilityElement class)
    public static ProbabilityElement<T> GetRandomWithProbability<T> (this IEnumerable<ProbabilityElement<T>> selections) {
        float upperBound = selections.Sum (p => p.probability);

        float rand = Random.Range (0f, upperBound);
        float currentProb = 0;
        foreach (var selection in selections) {
            currentProb += selection.probability;
            if (rand <= currentProb)
                return selection;
        }

        //This should not happen, but it makes sense to return the last one in this case
        return selections.Last ();
    }

    public static T GetRandomValue<T> (this IEnumerable<ProbabilityElement<T>> selections) {
        return selections.GetRandomWithProbability ().element;
    }

    public static string GetRandomString (uint lenght) {
        System.Random prnd = new System.Random ();
        var chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        return new string (chars.Select (c => chars[prnd.Next (chars.Length)]).Take ((int) lenght).ToArray ());
    }

    public static Vector2 Perlin2D (float amplitude, float frequency, int octaves, float persistance, float lacunarity, float burstFrequency) {
        float time = Time.time;
        float valX = 0;
        float valY = 0;

        float iAmplitude = 1;
        float iFrequency = frequency;
        float maxAmplitude = 0;

        // Burst frequency
        float burstCoord = time / (1 - burstFrequency);

        // Sample diagonally trough perlin noise
        float burstMultiplier = Mathf.PerlinNoise (burstCoord, burstCoord);

        //Apply contrast to the burst multiplier using power, it will make values stay close to zero and less often peak closer to 1
        burstMultiplier = Mathf.Pow (burstMultiplier, 2);

        for (int i = 0; i < octaves; i++) // Iterate trough octaves
        {
            float noiseFrequency = time / (1 - iFrequency) / 10;

            float perlinValueX = Mathf.PerlinNoise (noiseFrequency, 0.5f);
            float perlinValueY = Mathf.PerlinNoise (0.5f, noiseFrequency);

            // Adding small value To keep the average at 0 and   *2 - 1 to keep values between -1 and 1.
            perlinValueX = (perlinValueX + 0.0352f) * 2 - 1;
            perlinValueY = (perlinValueY + 0.0345f) * 2 - 1;

            valX += perlinValueX * iAmplitude;
            valY += perlinValueY * iAmplitude;

            // Keeping track of maximum amplitude for normalizing later
            maxAmplitude += iAmplitude;

            iAmplitude *= persistance;
            iFrequency *= lacunarity;
        }

        valX *= burstMultiplier;
        valY *= burstMultiplier;

        // normalize
        valX /= maxAmplitude;
        valY /= maxAmplitude;

        valX *= amplitude;
        valY *= amplitude;

        return new Vector2 (valX, valY);
    }
}

[System.Serializable]
public class PerlinParams {
    [Range (0, 100)]
    public float amplitude = 1;
    [Range (0.00001f, 0.99999f)]
    public float frequency = 0.98f;

    [Range (1, 4)]
    public int octaves = 2;

    [Range (0.00001f, 5)]
    public float persistance = 0.2f;
    [Range (0.00001f, 100)]
    public float lacunarity = 20;

    [Range (0.00001f, 0.99999f)]
    public float burstFrequency = 0.5f;

    public Vector2 GetPerlin2D () {
        return Randomness.Perlin2D (amplitude, frequency, octaves, persistance, lacunarity, burstFrequency);
    }

}

public class ProbabilityElement<T> {
    public T element;
    public float probability;

    public ProbabilityElement (T _element, float _probability) {
        element = _element;
        probability = _probability;
    }
}

/*
 * PseudoRandomDistribution container class. Used to create a PRD element, with avg probability p,
 * which guarantees to trigger after a certain amount of failed tries (N)
 */
public class PRD {
    private int N;
    private decimal C;

    public PRD (float p) {
        N = 0;
        C = CfromP ((decimal) p);
    }

    public float GetProbability () {
        N++;
        float p = Mathf.Clamp01 ((float) C * N);
        if (p >= 1f) N = 0;
        return p;
    }

    public bool CheckOccurrence (float rand) {
        if (GetProbability () > rand) {
            ResetTries ();
            return true;
        }
        return false;
    }

    public void ResetTries () { N = 0; }

    public decimal CfromP (decimal p) {
        decimal Cupper = p;
        decimal Clower = 0m;
        decimal Cmid;
        decimal p1;
        decimal p2 = 1m;
        while (true) {
            Cmid = (Cupper + Clower) / 2m;
            p1 = PfromC (Cmid);
            if (Math.Abs (p1 - p2) <= 0m) break;

            if (p1 > p)
                Cupper = Cmid;
            else
                Clower = Cmid;

            p2 = p1;
        }

        return Cmid;
    }

    private decimal PfromC (decimal C) {
        decimal pProcOnN = 0m;
        decimal pProcByN = 0m;
        decimal sumNpProcOnN = 0m;

        int maxFails = (int) System.Math.Ceiling (1m / C);
        for (int N = 1; N <= maxFails; ++N) {
            pProcOnN = System.Math.Min (1m, N * C) * (1m - pProcByN);
            pProcByN += pProcOnN;
            sumNpProcOnN += N * pProcOnN;
        }

        return (1m / sumNpProcOnN);
    }
}