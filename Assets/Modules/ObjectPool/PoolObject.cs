﻿using System.Collections;
using UnityEngine;

public class PoolObject : MonoBehaviour {
    public int instancesOnFirstPool = 5;
    public int instancesOnNewPool = 3;
    [System.NonSerialized]
    public Transform poolHolder;
    [System.NonSerialized]
    public int poolKey;
    [System.NonSerialized]
    public bool parentIsPooledObject = false;

    public virtual GameObject Instantiate (Vector3 position, Quaternion rotation) {
        return PoolManager.instance.ReuseObject (this, position, rotation);
    }
    public virtual GameObject Instantiate () {
        return PoolManager.instance.ReuseObject (this);
    }

    public virtual void Reuse (Vector3 position, Quaternion rotation) {
        transform.SetParent (null, false);
        transform.position = position;
        transform.rotation = rotation;
        gameObject.SetActive (true);
    }

    /// <summary>
    /// Disable object and send it back to the pool. Recommend to always be called with a delay.
    /// </summary>
    public virtual void Destroy (float delay = 0f) {
        if (parentIsPooledObject) return;

        transform.SetParent (null, false);
        if (delay != 0 && gameObject.activeSelf)
            StartCoroutine (DestroyWithDelay (delay));
        else {
            gameObject.SetActive (false);
            PoolManager.instance.poolDictionary[poolKey].Enqueue (this);
            transform.SetParent (poolHolder, false);
        }
    }

    IEnumerator DestroyWithDelay (float delay) {
        yield return new WaitForSeconds (delay);
        Destroy ();
    }
}