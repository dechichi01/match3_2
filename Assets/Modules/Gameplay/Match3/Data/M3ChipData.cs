﻿using UnityEngine;

[CreateAssetMenu (menuName = "ScriptableObject/M3ChipData")]
public class M3ChipData : ScriptableObject {

    [SerializeField] private ChipColor _color;
    [SerializeField] private Sprite _sprite;
    [SerializeField][Range (0, 1)] private float _probability = 0.1f;

    public ChipColor Color { get { return _color; } }
    public Sprite Sprite { get { return _sprite; } }

    public float Probability { get { return _probability; } }
}

public enum ChipColor { Green, Red, Blue, Dark, Light, Heart }