namespace Match3 {
    public interface IM3MapDelegatorDependent {
        M3MapDelegator MapDelegator { set; }
    }

    public interface IM3MapUIDependent {
        M3MapUI MapUI { set; }
    }
}