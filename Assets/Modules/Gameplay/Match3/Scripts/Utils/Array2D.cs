﻿using System;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Array2D<T> : IEnumerable<T> { //TODO extend ICollection

	private T[] _arr;
	private int _lineCount;
	private int _columnCount;

	public int LineCount { get { return _lineCount; } }
	public int ColumnCount { get { return _columnCount; } }

	public int Size { get { return _lineCount * _columnCount; } }

	public int MaxDimension { get { return _lineCount > _columnCount ? _lineCount : _columnCount; } }
	public Array2D (int lines, int columns) {
		_lineCount = lines;
		_columnCount = columns;
		_arr = new T[_lineCount * _columnCount];
	}

	public Array2D (T[] arr) {
		_arr = new T[arr.Length];
		Array.Copy (arr, _arr, arr.Length);
	}

	#region Iterations
	public void ForEach2D (Action<int, int, T> action) {
		if (action == null) return;

		for (int index = 0; index < _arr.Length; index++) {
			int i = LineFromIndex (index);
			int j = ColumnFromIndex (index);
			action (i, j, _arr[index]);
		}
	}

	public void ForEachAssign2D (Func<int, int, T, T> action) {
		if (action == null) return;

		for (int index = 0; index < _arr.Length; index++) {
			int i = LineFromIndex (index);
			int j = ColumnFromIndex (index);
			_arr[index] = action (i, j, _arr[index]);
		}
	}

	public void ForEach (Action<int, T> action) {
		if (action == null) return;

		for (int index = 0; index < _arr.Length; index++) {
			action (index, _arr[index]);
		}
	}

	public void ForEachAssign (Func<int, T, T> action) {
		if (action == null) return;

		for (int index = 0; index < _arr.Length; index++) {
			_arr[index] = action (index, _arr[index]);
		}
	}
	#endregion

	public T SafeGet (int i) {
		if (i >= _arr.Length || i < 0) return default (T);
		return _arr[i];
	}

	public T SafeGet (int i, int j) {
		return SafeGet (IndexFromIJ (i, j));
	}

	public T[] GetSurroundings (int i, int j) {
		int cLeft = j - 1;
		int cRight = j + 1;
		int lTop = i - 1;
		int lBottom = i + 1;

		int columnCount = _columnCount;
		int lineCount = _lineCount;

		T left = SafeGet (i, cLeft);
		T right = SafeGet (i, cRight);
		T top = SafeGet (lTop, j);
		T bottom = SafeGet (lBottom, j);

		T bottomLeft = SafeGet (lBottom, cLeft);
		T topLeft = SafeGet (lTop, cLeft);
		T topRight = SafeGet (lTop, cRight);
		T bottomRight = SafeGet (lBottom, cRight);

		return new T[] { left, top, right, bottom, bottomLeft, topLeft, topRight, bottomRight };
	}

	public int LineFromIndex (int index) {
		return index / _columnCount;
	}

	public int ColumnFromIndex (int index) {
		return index % _columnCount;
	}

	public int IndexFromIJ (int i, int j) {
		return i * _columnCount + j;
	}

	#region IEnumerable<T>
	public IEnumerator<T> GetEnumerator () {
		return ((IEnumerable<T>) _arr).GetEnumerator ();
	}

	IEnumerator IEnumerable.GetEnumerator () {
		return _arr.GetEnumerator ();
	}
	#endregion

	#region Overloads
	public T this [int i] {
		get {
			return _arr[i];
		}
		set {
			_arr[i] = value;
		}
	}

	public T this [int i, int j] {
		get {
			return _arr[i * _columnCount + j];
		}
		set {
			_arr[i * _columnCount + j] = value;
		}
	}
	#endregion
}