using UnityEngine;
public static class Polynomials {

    //Calculate NPoly value for x and a series of coeficientes (a0...an) where the equation looks like: y = a0 + a1*x + a2*x^2... + an*x^(n-1)
    public static float CalculatePolyNValue (float x, params float[] cs) {
        int power = 0;
        float result = 0;
        foreach (var c in cs) {
            result += Mathf.Pow (x, power) * c;
            power++;
        }

        return result;
    }
}