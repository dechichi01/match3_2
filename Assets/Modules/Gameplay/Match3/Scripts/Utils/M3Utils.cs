﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Match3 {
    public static class ArrayUtils {
        public static int[] CreateRange (int start, int count) {
            int[] retVal = new int[count];
            for (int i = 0; i < count; i++) {
                retVal[i] = start + i;
            }

            return retVal;
        }
    }

    public static class EnumUtils {
        public static SlotSides GetOppositSide (this SlotSides side) {
            return (SlotSides) ((int) (side + 2) % 4);
        }

        public static SlotSides[] GetPerpendiculars (this SlotSides side) {
            var perpendicular = (SlotSides) ((int) (side + 1) % 4);
            return new SlotSides[] { perpendicular, perpendicular.GetOppositSide () };
        }
    }
}