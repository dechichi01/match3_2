﻿using System.Linq;
using MoreLinq;
using UnityEngine;

namespace Match3 {
	[RequireComponent (typeof (FingerDragDetector))]
	public class M3MapInputHandler : MonoBehaviour, IM3MapUIDependent {

		public M3MapUI MapUI { private get; set; }

		[SerializeField] private Canvas _canvas;

		[SerializeField] private M3PlayerActionHandler _playerActionHandler;
		[SerializeField] private InputRectArea _movementBounds;

		private FingerDragDetector _fingerDragDetect;

		private M3MapSlotUI _currentPressedSlot = null;
		private Vector2 _startScreenOffset = Vector2.zero;

		private void Awake () {
			_fingerDragDetect = GetComponent<FingerDragDetector> ();
		}

		private void OnEnable () {
			_fingerDragDetect.OnFingerDown += OnFingerDown;
			_fingerDragDetect.OnFingerUp += OnFingerUp;
			_fingerDragDetect.OnFingerDrag += OnFingerDrag;
		}

		private void OnDisable () {
			_fingerDragDetect.OnFingerDown -= OnFingerDown;
			_fingerDragDetect.OnFingerUp -= OnFingerUp;
			_fingerDragDetect.OnFingerDrag -= OnFingerDrag;
		}

		private void OnFingerDown (Vector2 screenPosition) {
			if (_currentPressedSlot != null) {
				return;
			}

			Vector2 uiPoint = ScreenToWorldPoint (screenPosition);

			_currentPressedSlot = MapUI.SlotsUIs
				.Where (s => s.InputArea.ContainsScreenPoint (screenPosition))
				.MinByOrDefault (s => s.InputArea.SqrDistanceToUIPoint (uiPoint));

			if (_currentPressedSlot != null) {
				SetMovementBounds (_currentPressedSlot.InputArea.Area);
				var slotScreenPos = WorldToScreenPoint (_currentPressedSlot.transform.position);
				_startScreenOffset = screenPosition - slotScreenPos;
				_playerActionHandler.OnChipSelectedInSlot (_currentPressedSlot, _currentPressedSlot.transform.position);
			}
		}

		private void OnFingerUp (Vector2 screenPosition) {
			if (_currentPressedSlot == null) {
				return;
			}

			_playerActionHandler.OnChipReleased (_currentPressedSlot, ScreenToWorldPoint (screenPosition));
			_currentPressedSlot = null;
		}

		private void OnFingerDrag (Vector2 screenPosition, Vector2 screenDelta) {
			if (_currentPressedSlot == null) {
				return;
			}

			var finalScreenPoint = screenPosition - _startScreenOffset;

			if (!_movementBounds.ContainsScreenPoint (finalScreenPoint)) {
				OnFingerUp (screenPosition);
			} else {
				_playerActionHandler.OnChipMove (ScreenToWorldPoint (finalScreenPoint));
			}
		}

		private void SetMovementBounds (RectTransform rect) {
			var boundsArea = _movementBounds.Area;
			boundsArea.sizeDelta = 3 * rect.sizeDelta - 0.9f * 0.5f * rect.sizeDelta;
			boundsArea.position = rect.position;
		}

		private Vector2 ScreenToWorldPoint (Vector2 screenPoint) {
			Vector2 retVal;
			RectTransformUtility.ScreenPointToLocalPointInRectangle (_canvas.transform as RectTransform, screenPoint, _canvas.worldCamera, out retVal);
			return _canvas.transform.TransformPoint (retVal);
		}

		private Vector2 WorldToScreenPoint (Vector2 uiPoint) {
			return RectTransformUtility.WorldToScreenPoint (_canvas.worldCamera, uiPoint);
		}
	}
}