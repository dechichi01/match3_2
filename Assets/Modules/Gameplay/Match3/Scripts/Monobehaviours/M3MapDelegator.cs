﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Match3 {
	public class M3MapDelegator : MonoBehaviour {

		[SerializeField] private bool _useSeed = false;
		[SerializeField] private int _seed;

		[SerializeField] private M3ChipGetter _chipGetter;

		public const int _lineCount = 5;
		public const int _columnCount = 7;

		#region M3Logic
		private M3Map _map;

		#endregion

		private System.Random _prg;
		private System.Random Prg { get { return _prg ?? (_prg = NewPRG ()); } }

		private System.Random NewPRG () {
			return _useSeed ? new System.Random (_seed) : new System.Random ();
		}

		#region PublicVariables
		public int LineCount { get { return _lineCount; } }
		public int ColumnCount { get { return _columnCount; } }
		#endregion

		private void Initialize () {
			if (_map != null) return;
			_map = new M3Map (_chipGetter, Prg);
		}

		public Array2D<M3MapSlot> GenerateNewMap () {
			if (_map == null) Initialize ();
			return _map.GenerateNewMap (_lineCount, _columnCount);
		}

		public void SwapChips (M3MapSlot a, M3MapSlot b) {
			_map.SwapChipInSlots (a, b);
		}

		public void CopyFromUI (Array2D<M3MapSlotUI> slotsUI) {
			_map.Slots.ForEach ((i, s) => {
				var sUI = slotsUI[i];
				s.SetChip (sUI.CurrentChip != null ? sUI.CurrentChip.ChipData : null);
			});
		}

		public M3MapSlot[][] SolutionsForSwappingSlots (M3MapSlot a, M3MapSlot b) {
			var solutions = _map.SwapSlotsAndFindSolutions (a, b);
			if (solutions == null) return null;

			foreach (var solution in solutions) {
				for (int i = 0; i < solution.Length; i++) {
					if (solution[i] == a) solution[i] = b;
					else if (solution[i] == b) solution[i] = a;
				}
			}

			return solutions;
		}

		public List<M3MapSlot[]> SolutionForSlots (params M3MapSlot[] slots) {
			List<M3MapSlot[]> solutions = new List<M3MapSlot[]> ();
			foreach (var s in slots) {
				var solution = _map.FindSolutionForSlot (s);
				if (solution != null) {
					solutions.Add (solution);
				}
			}

			return solutions;
		}
	}
}