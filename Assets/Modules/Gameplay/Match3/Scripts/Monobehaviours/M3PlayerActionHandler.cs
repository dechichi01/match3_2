﻿using System.Collections;
using System.Collections.Generic;
using MoreLinq;
using UnityEngine;

namespace Match3 {
	public class M3PlayerActionHandler : MonoBehaviour, IM3MapDelegatorDependent, IM3MapUIDependent {

		public M3MapDelegator MapDelegator { private get; set; }
		public M3MapUI MapUI { private get; set; }

		[SerializeField] private M3MapChipMover _chipMover;
		[SerializeField][Range (0, 1)] private float _minPercentToConsiderMoved = 0.55f;
		private float _minAngleDiffPercentToChangeNeighbour = 0.08f;

		private M3MapSlotUI _currentSlot;
		private RectTransform _currentSlotRect;

		private M3MapSlotUI _lastMinAngleNeighbour = null;
		private M3MapSlotUI _currentExchangingNeighbour = null;
		private List<M3MapSlot> _currentAffectedChips = new List<M3MapSlot> ();

		#region ActionCallbacks
		public void OnChipSelectedInSlot (M3MapSlotUI slot, Vector2 point) {
			if (_currentSlot != null || slot == null) {
				return;
			}

			_chipMover.CalculateSqrSlotCenterDistance (slot);
			_currentSlot = slot;
			_currentSlotRect = _currentSlot.InputArea.Area;
			_currentSlot.ReleaseCurrentChip ();

			_chipMover.MoveChipWithScale (_currentSlot.CurrentChip, _currentSlotRect.position, point);

			_currentSlot.OnPressed ();
		}

		public void OnChipMove (Vector2 point) {
			_chipMover.MoveChipWithScale (_currentSlot.CurrentChip, _currentSlotRect.position, point);

			var slotToPoint = point - (Vector2) _currentSlotRect.position;
			var minAngleSlot = _currentSlot.Surrounding8.MinBy (s => s == null ? float.MaxValue : Vector2.Angle (slotToPoint, (s.transform.position - _currentSlotRect.position)));
			if (minAngleSlot != null && minAngleSlot != _lastMinAngleNeighbour && CanChangeTargetNeighbour (point, minAngleSlot)) {
				ChangeMinAngleNeighbour (minAngleSlot);
				var solutions = MapDelegator.SolutionsForSwappingSlots (_currentSlot.Slot, minAngleSlot.Slot);
				if (solutions != null) {
					foreach (var s in solutions) {
						_currentAffectedChips.AddRange (s);
						MapUI.DoPossibleSolutionFeedback (s);
					}

					_currentExchangingNeighbour = minAngleSlot;
					_chipMover.StartChipSwapWithNeighbour (_currentSlot, _currentExchangingNeighbour);
				}
			}
		}

		public void OnChipReleased (M3MapSlotUI slotUI, Vector2 point) {
			if (_currentSlot == null) {
				return;
			}

			if (_currentExchangingNeighbour != null && _chipMover.PercentTravelledByChip (_currentSlot, _currentExchangingNeighbour) >= _minPercentToConsiderMoved) {
				StartCoroutine (OnSolutionFound ());
				_currentExchangingNeighbour = null;
				_currentSlot = null;
				_currentSlotRect = null;
				_lastMinAngleNeighbour = null;
			} else {
				ChangeMinAngleNeighbour (null);

				_chipMover.LerpChipToPosition (_currentSlot.CurrentChip, _currentSlotRect.position, () => {
					_currentSlot.CurrentChip.transform.localScale = Vector3.one;
					_currentSlot.SetChip (_currentSlot.CurrentChip);
					_currentSlot = null;
					_currentSlotRect = null;
					_lastMinAngleNeighbour = null;
				});
			}
		}

		#endregion

		#region NeighbourSwapHandler
		private bool CanChangeTargetNeighbour (Vector2 point, M3MapSlotUI newMinAngleNeighbour) {
			if (_lastMinAngleNeighbour == null || _currentExchangingNeighbour == null) return true;
			var slotToPoint = point - (Vector2) _currentSlotRect.position;
			var newNeighbourAngle = Vector2.Angle (newMinAngleNeighbour.transform.position - _currentSlotRect.position, slotToPoint);
			var lastNeighbourAngle = Vector2.Angle (_lastMinAngleNeighbour.transform.position - _currentSlotRect.position, slotToPoint);

			return 1f - (newNeighbourAngle / lastNeighbourAngle) >= _minAngleDiffPercentToChangeNeighbour;
		}

		private void ChangeMinAngleNeighbour (M3MapSlotUI newMinAngle) {
			if (_currentExchangingNeighbour != null) {
				_chipMover.EndChipSwapWithNeighbour ();
				EndFeedBacksOnAffectedChips ();

				var neighbour = _currentExchangingNeighbour;
				_currentExchangingNeighbour = null;

				_chipMover.LerpChipToPosition (neighbour.CurrentChip, neighbour.InputArea.Area.position,
					() => neighbour.SetChip (neighbour.CurrentChip));
			}

			_lastMinAngleNeighbour = newMinAngle;
		}
		#endregion

		#region OnActionFinished
		private void EndFeedBacksOnAffectedChips () {
			MapUI.EndFeedBacks (_currentAffectedChips.ToArray ());
			_currentAffectedChips.Clear ();
		}

		private IEnumerator OnSolutionFound () {
			_chipMover.EndChipSwapWithNeighbour ();
			EndFeedBacksOnAffectedChips ();

			var currentSlot = _currentSlot;
			var slotChip = currentSlot.CurrentChip;

			var neighbourSlot = _currentExchangingNeighbour;
			var neighbourChip = neighbourSlot.CurrentChip;

			int finishCount = 0;

			_chipMover.LerpChipToPosition (slotChip, neighbourSlot.transform.position, () => {
				slotChip.transform.localScale = Vector3.one;
				neighbourSlot.SetChip (slotChip);
				finishCount++;
			});

			_chipMover.LerpChipToPosition (neighbourChip, currentSlot.transform.position, () => {
				neighbourChip.transform.localScale = Vector3.one;
				currentSlot.SetChip (neighbourChip);
				finishCount++;
			});

			while (finishCount < 2) { yield return null; }

			MapDelegator.SwapChips (currentSlot.Slot, neighbourSlot.Slot);
			var solutions = MapDelegator.SolutionForSlots (currentSlot.Slot, neighbourSlot.Slot);
			foreach (var s in solutions) { MapUI.DoSolutionFoundFeedback (s); }
			foreach (var s in solutions) { MapUI.DestroyChipsInSlots (s); }
		}
		#endregion
	}

}