﻿using System.Collections.Generic;
using UnityEngine;

namespace Match3 {
    public class M3ChipGetter : MonoBehaviour {

        [SerializeField] private M3ChipData[] _chips;

        private Dictionary<ChipColor, M3ChipData> _chipsDict;
        private Dictionary<ChipColor, M3ChipData> ChipsDict {
            get {
                if (_chipsDict != null) return _chipsDict;
                BuildChipsDict ();
                return _chipsDict;
            }
        }

        private ProbabilityElement<ChipColor>[] _allColors;
        private ProbabilityElement<ChipColor>[] AllColors {
            get {
                if (_allColors != null && _allColors.Length > 0) return _allColors;
                BuildColorsProbabilities ();
                return _allColors;
            }
        }

        private void BuildColorsProbabilities () {
            _allColors = new ProbabilityElement<ChipColor>[_chips.Length];
            for (int i = 0; i < _chips.Length; i++) {
                var chip = _chips[i];
                _allColors[i] = new ProbabilityElement<ChipColor> (chip.Color, chip.Probability);
            }
        }
        private void BuildChipsDict () {
            _chipsDict = new Dictionary<ChipColor, M3ChipData> ();
            foreach (var chip in _chips) {
                _chipsDict.Add (chip.Color, chip);
            }
        }

        public M3ChipData GetRandomChip () {
            ChipColor randomColor = AllColors.GetRandomValue ();
            return ChipsDict[randomColor];
        }

        public M3ChipData GetRandomChip (M3MapSlot[] surroundings) {
            List<ProbabilityElement<ChipColor>> possibleColors = new List<ProbabilityElement<ChipColor>> (AllColors.Length);
            foreach (ProbabilityElement<ChipColor> color in AllColors) {
                bool exclude = false;
                foreach (var slot in surroundings) {
                    if (slot != null && slot.CurrentChip != null && slot.CurrentChip.Color == color.element) {
                        exclude = true;
                        break;
                    }
                }

                if (!exclude) possibleColors.Add (color);
            }

            ChipColor randomColor = possibleColors.Count > 0 ? possibleColors.GetRandomValue () : AllColors.GetRandomValue ();
            return ChipsDict[randomColor];
        }

        public M3ChipData[] GetRandomChips (int count, int maxRepeat = int.MaxValue) {
            Dictionary<ChipColor, int> repeatsDict = new Dictionary<ChipColor, int> ();
            List<ProbabilityElement<ChipColor>> possibleColors = new List<ProbabilityElement<ChipColor>> (AllColors);

            M3ChipData[] retVal = new M3ChipData[count];

            for (int i = 0; i < count; i++) {
                ProbabilityElement<ChipColor> probabilityElement;
                ChipColor color;

                if (possibleColors.Count > 0) {
                    probabilityElement = possibleColors.GetRandomWithProbability ();
                    color = probabilityElement.element;
                    int repeats = repeatsDict.ContainsKey (color) ? repeatsDict[color] + 1 : 1;
                    repeatsDict[color] = repeats;
                    if (repeats >= maxRepeat) {
                        possibleColors.Remove (probabilityElement);
                    }
                } else {
                    probabilityElement = AllColors.GetRandomWithProbability ();
                    color = probabilityElement.element;
                }

                retVal[i] = ChipsDict[color];
            }

            return retVal;
        }
    }

}