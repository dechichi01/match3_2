﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Match3 {
    public class M3MapUI : MonoBehaviour, IM3MapDelegatorDependent {

        public M3MapDelegator MapDelegator { private get; set; }

        [SerializeField] private M3ChipUIGravity _chipGravity;
        [SerializeField] private M3MapSlotUI _slotPrefab;
        [SerializeField] private M3ChipUI _chipPrefab;
        [SerializeField] private GridLayoutGroup _grid;

        [SerializeField] private Array2D<M3MapSlotUI> _slotsUIs;

        public Array2D<M3MapSlotUI> SlotsUIs { get { return _slotsUIs; } }

        #region Monobehaviour
        private void Start () {
            GenerateMap ();
            _chipGravity.OnFallFinished += OnChipFallFinished;
        }

        private void OnDestroy () {
            _chipGravity.OnFallFinished -= OnChipFallFinished;
        }
        private void OnValidate () {
            if (_slotsUIs != null && _slotsUIs.Size > transform.childCount) {
                _slotsUIs = null;
            }
        }
        #endregion

        [ContextMenu ("Generate Map")]
        public void GenerateMap () {
            GenerateSlotsIfNeeded ();
            var slotsData = MapDelegator.GenerateNewMap ();

            slotsData.ForEach2D ((i, j, slot) => {
                var slotUI = _slotsUIs[slotsData.IndexFromIJ (i, j)];
                slotUI.name = string.Format ("Slot ({0}, {1})", i, j);
                slotUI.Initialize (slotsData[i, j]);
                slotUI.CurrentChip.DebugBackColor ();
            });

            _slotsUIs.ForEach ((i, s) => {
                s.SetSurroundings (_slotsUIs);
            });
        }

        [ContextMenu ("Clear Map")]
        private void ClearMap () {
            while (transform.childCount > 0) {
                DestroyImmediate (transform.GetChild (0).gameObject);
            }

            _slotsUIs = null;
        }

        private Array2D<M3MapSlotUI> GenerateSlotsIfNeeded () {
            var lineCount = MapDelegator.LineCount;
            var columnCount = MapDelegator.ColumnCount;
            if (_slotsUIs != null && _slotsUIs.Size == lineCount * columnCount && transform.childCount >= _slotsUIs.Size) {
                return _slotsUIs;
            }

            ClearMap ();
            _slotsUIs = new Array2D<M3MapSlotUI> (lineCount, columnCount);

            for (int i = 0; i < _slotsUIs.Size; i++) {
                var slot = Instantiate (_slotPrefab);
                slot.transform.SetParent (_grid.transform, false);
                slot.gameObject.SetActive (true);

                var chip = _chipPrefab.Instantiate ().GetComponent<M3ChipUI> ();
                chip.gameObject.SetActive (true);
                slot.SetChip (chip);
                _slotsUIs[i] = slot;
            }

            _chipGravity.SetChips (_slotsUIs);
            return _slotsUIs;
        }

        public void DestroyChipsInSlots (M3MapSlot[] slots) {
            SlotsToSlotUIsLoop (slots, s => {
                s.DestroyCurrentChip ();
            });

            _chipGravity.ApplyGravity ();
        }

        private void OnChipFallFinished () {
            MapDelegator.CopyFromUI (_slotsUIs);
        }

        #region UIFeedbacks
        public void SlotsToSlotUIsLoop (M3MapSlot[] slots, System.Action<M3MapSlotUI> action) {
            if (action == null) return;

            foreach (var slot in slots) {
                action (_slotsUIs.SafeGet (slot.Index));
            }
        }
        public void DoPossibleSolutionFeedback (M3MapSlot[] slots) {
            SlotsToSlotUIsLoop (slots, s => s.PossibleSolutionFeedback ());
        }

        public void DoSolutionFoundFeedback (M3MapSlot[] slots) {
            SlotsToSlotUIsLoop (slots, s => s.SolutionFoundFeedback ());
        }

        public void EndFeedBacks (M3MapSlot[] slots) {
            SlotsToSlotUIsLoop (slots, s => s.EndEffects ());
        }
        #endregion
    }
}