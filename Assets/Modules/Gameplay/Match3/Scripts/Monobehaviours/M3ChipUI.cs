﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Match3 {
    [RequireComponent (typeof (RectTransform))]
    public class M3ChipUI : PoolObject {

        [SerializeField] private Image _image;
        [SerializeField][Range (0, 1)] private float _uniformAnchorDelta = 0.08f;

        private M3ChipData _chipData;
        public M3ChipData ChipData { get { return _chipData; } }

        public M3MapSlotUI ParentSlot { get; private set; }
        private RectTransform _thisRect;
        public RectTransform ThisRect { get { return _thisRect ?? (_thisRect = GetComponent<RectTransform> ()); } }

        public void Initialize (M3ChipData data) {
            _chipData = data;
            _image.sprite = data.Sprite;
        }

        public void SetParentSlot (M3MapSlotUI slot) {
            ParentSlot = slot;
            if (slot == null) return;
            transform.SetParent (slot.transform, false);
            ThisRect.anchoredPosition = Vector2.zero;
            ThisRect.anchorMax = Vector2.one * (1 - _uniformAnchorDelta);
            ThisRect.anchorMin = Vector2.one * _uniformAnchorDelta;

            ThisRect.offsetMax = Vector2.zero;
            ThisRect.offsetMin = Vector2.zero;
        }

        public void LeaveParentSlot () {
            if (ParentSlot == null) return;
            ParentSlot.ReleaseCurrentChip ();
            ParentSlot.SetChip (null);
            ParentSlot = null;
        }

        public override void Destroy (float delay = 0) {
            //Destroy anim
            base.Destroy ();
        }
        public void DebugSetColor () {
            _image.color = Color.black;
        }

        public void DebugBackColor () {
            _image.color = Color.white;
        }
    }
}