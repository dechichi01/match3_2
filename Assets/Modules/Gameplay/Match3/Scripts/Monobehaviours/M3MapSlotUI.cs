﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Match3 {
    [RequireComponent (typeof (RectTransform))]
    public class M3MapSlotUI : MonoBehaviour {

        [SerializeField] private Image _bgSprite;
        [SerializeField] private Color _evenColor;
        [SerializeField] private Color _oddColor;

        [HideInInspector][SerializeField] private M3MapSlot _slot;
        [HideInInspector][SerializeField] private M3ChipUI _currentChip;
        [SerializeField] private InputRectArea _inputArea;
        public InputRectArea InputArea { get { return _inputArea; } }

        public M3ChipUI CurrentChip { get { return _currentChip; } }

        public M3MapSlotUI[] Surrounding4 { get; private set; }
        public M3MapSlotUI[] Surrounding8 { get; private set; }

        public M3MapSlot Slot { get { return _slot; } }

        public void Initialize (M3MapSlot slot) {
            _slot = slot;
            _currentChip.Initialize (_slot.CurrentChip);
            UpdateUI ();
        }

        public void SetChip (M3ChipUI chip) {
            if (_currentChip != null && _currentChip.ParentSlot == this) {
                _currentChip.SetParentSlot (null);
            }

            _currentChip = chip;
            if (chip == null) return;

            _currentChip.SetParentSlot (this);
        }

        public void SetSurroundings (Array2D<M3MapSlotUI> allSlots) {
            Surrounding8 = allSlots.GetSurroundings (_slot.I, _slot.J);
            Surrounding4 = new M3MapSlotUI[4];
            System.Array.Copy (Surrounding8, Surrounding4, 4);
        }

        private void UpdateUI () {
            bool evenPos = _slot.Index % 2 == 0;
            _bgSprite.color = evenPos ? _evenColor : _oddColor;
        }

        public void ReleaseCurrentChip () {
            _currentChip.transform.SetParent (transform.parent, true);
            _currentChip.transform.SetAsLastSibling ();
        }

        public void DestroyCurrentChip () {
            _currentChip.Destroy ();
            SetChip (null);
        }

        public void OnPressed () {
            //play anim
        }

        public void OnReleased () {
            //play anim
        }

        public void PossibleSolutionFeedback () {
            _currentChip.DebugSetColor ();
        }

        public void SolutionFoundFeedback () {

        }

        public void EndEffects () {
            _currentChip.DebugBackColor ();
        }
    }
}