﻿using System.Collections;
using System.Collections.Generic;
using MoreLinq;
using UnityEngine;

namespace Match3 {
	public class M3MapChipMover : MonoBehaviour {

		[SerializeField] private AnimationCurve _scaleThroughMovementAnim;
		[SerializeField] private float _returnChipTime = 0.15f;

		private Coroutine _exchangeNeighbourCoroutine = null;
		private float _sqrSlotCenterDistance = -1;

		public void CalculateSqrSlotCenterDistance (M3MapSlotUI slot) {
			if (_sqrSlotCenterDistance > 0 || slot == null) {
				return;
			}

			foreach (var s in slot.Surrounding4) {
				if (s != null) {
					_sqrSlotCenterDistance = (slot.transform.position - s.transform.position).sqrMagnitude;
					break;
				}
			}
		}

		#region ChipMovement
		public void MoveChipWithScale (M3ChipUI chip, Vector2 startPos, Vector2 point) {
			float traveledSqrDist = (point - startPos).sqrMagnitude;
			chip.transform.localScale = Vector3.one * GetScaleFactorForSqrDistance (traveledSqrDist);
			chip.transform.position = point;
		}

		public void LerpChipToPosition (M3ChipUI chipUI, Vector2 endPoint, System.Action onFinished = null) {
			StartCoroutine (PerformLerpChipToPosition (chipUI, endPoint, _returnChipTime, onFinished));
		}
		private float GetScaleFactorForSqrDistance (float sqrDist) {
			float percent = sqrDist / _sqrSlotCenterDistance;
			return _scaleThroughMovementAnim.Evaluate (percent);
		}

		private IEnumerator PerformLerpChipToPosition (M3ChipUI chipUI, Vector2 endPoint, float time, System.Action onFinished) {
			float speed = 1f / time;
			float percent = 0;
			Vector2 startPoint = chipUI.transform.position;
			while (percent < 1) {
				percent += speed * Time.deltaTime;
				chipUI.transform.position = Vector2.Lerp (startPoint, endPoint, percent);
				yield return null;
			}

			chipUI.transform.position = endPoint;

			if (onFinished != null) {
				onFinished ();
			}
		}

		#endregion

		#region ChipSwapMovement

		public void StartChipSwapWithNeighbour (M3MapSlotUI currentSlot, M3MapSlotUI neighbour) {
			EndChipSwapWithNeighbour ();
			neighbour.ReleaseCurrentChip ();
			currentSlot.ReleaseCurrentChip ();
			_exchangeNeighbourCoroutine = StartCoroutine (SwapChipWithCurrentNeighbour (currentSlot, neighbour));
		}

		public void EndChipSwapWithNeighbour () {
			if (_exchangeNeighbourCoroutine != null) {
				StopCoroutine (_exchangeNeighbourCoroutine);
			}
		}

		private IEnumerator SwapChipWithCurrentNeighbour (M3MapSlotUI currentSlot, M3MapSlotUI neighbour) {
			var neighbourChipRect = neighbour.CurrentChip.ThisRect;
			Vector2 neighbourPos = neighbourChipRect.position;
			Vector2 currSlotPos = currentSlot.transform.position;
			float totalSqrDistance = ((Vector2) currSlotPos - neighbourPos).sqrMagnitude;
			float maxSpeed = 1.2f * Mathf.Pow (totalSqrDistance, 0.5f) / _returnChipTime;
			while (true) {
				var percent = PercentTravelledByChip (currentSlot, neighbour);
				var newPos = Vector2.Lerp (neighbourPos, currSlotPos, percent);
				neighbourChipRect.position = Vector2.MoveTowards (neighbourChipRect.position, newPos, Time.deltaTime * maxSpeed);
				yield return null;
			}
		}

		public float PercentTravelledByChip (M3MapSlotUI currSlot, M3MapSlotUI targetSlot) {
			return (currSlot.CurrentChip.ThisRect.position - currSlot.transform.position).sqrMagnitude / (targetSlot.transform.position - currSlot.transform.position).sqrMagnitude;
		}

		#endregion
	}
}