﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Match3 {
    [RequireComponent (typeof (RectTransform))]
    [ExecuteInEditMode]
    public class M3BoardUI : MonoBehaviour {

        [SerializeField] private RectTransform _parentRect;
        [SerializeField] private LayoutElement _mapLayoutElement;
        [SerializeField] private GridLayoutGroup _mapGrip;
        [SerializeField] private LayoutElement _healthBarLayoutElement;
        [SerializeField][Range (0, 1)] private float _healthBarBoardProportion = 0.05f;

        private RectTransform _rect;
        private RectTransform ThisRect { get { return _rect ?? (_rect = GetComponent<RectTransform> ()); } }

        private void OnValidate () {
            SetMinHeights ();
        }

        private void OnRectTransformDimensionsChange () {
            SetMinHeights ();
        }

        private void SetMinHeights () {
            if (_mapLayoutElement == null || _mapGrip == null || _healthBarLayoutElement == null) return;

            var rect = ThisRect.rect;

            var width = rect.width;

            var gridWidth = width - (_mapGrip.padding.left + _mapGrip.padding.right);
            var tileSize = (gridWidth / M3MapDelegator._columnCount) - _mapGrip.spacing.x;

            _mapGrip.cellSize = Vector2.one * tileSize;

            var mapHeight = (tileSize + _mapGrip.spacing.y) * M3MapDelegator._lineCount + _mapGrip.padding.top + _mapGrip.padding.bottom;
            _mapLayoutElement.minHeight = mapHeight;

            var healthBarHeight = mapHeight * (1 + _healthBarBoardProportion) - mapHeight;
            _healthBarLayoutElement.minHeight = healthBarHeight;

            var relativeHeightToParent = (mapHeight + healthBarHeight) / _parentRect.rect.height;

            ThisRect.anchorMax = new Vector2 (ThisRect.anchorMax.x, relativeHeightToParent);
        }
    }
}