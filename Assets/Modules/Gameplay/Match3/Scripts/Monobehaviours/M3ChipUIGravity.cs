﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Match3 {
	public class M3ChipUIGravity : MonoBehaviour {

		[SerializeField] private float _gravity = 2500f;
		[SerializeField] private float _startSpeed = 250f;
		private ChipMovementInfo[] _chipsMovementInfo;

		private bool _applyingGravity = false;

		public event System.Action OnFallFinished;
		public void SetChips (Array2D<M3MapSlotUI> slots) {
			_chipsMovementInfo = new ChipMovementInfo[slots.Size];
			slots.ForEach ((i, s) => _chipsMovementInfo[i] = new ChipMovementInfo (s.CurrentChip));
		}

		public void ApplyGravity () {
			if (_applyingGravity) {
				return;
			}

			StartCoroutine (PerformApplyGravity ());
		}

		private IEnumerator PerformApplyGravity () {
			int chipCount = _chipsMovementInfo.Length;
			bool continueLooping = true;
			_applyingGravity = true;
			while (continueLooping) {
				continueLooping = false;

				for (int i = 0; i < chipCount; i++) {
					var movementInfo = _chipsMovementInfo[i];
					var parentSlot = movementInfo.chipUI.ParentSlot;

					if (movementInfo.targetSlot != null) {
						continueLooping = true;
						ChipFall (movementInfo);
					} else if (parentSlot != null) {
						var bottom = parentSlot.Surrounding4[(int) SlotSides.Bottom];
						if (bottom != null && bottom.CurrentChip == null) {
							continueLooping = true;
							movementInfo.chipUI.LeaveParentSlot ();
							movementInfo.targetSlot = bottom;
							movementInfo.currSpeed = _startSpeed;
						}
					}
				}

				yield return null;
			}

			_applyingGravity = false;

			if (OnFallFinished != null) {
				OnFallFinished ();
			}
		}

		private void ChipFall (ChipMovementInfo movementInfo) {
			var chipUITrans = movementInfo.chipUI.transform;
			var targetSlot = movementInfo.targetSlot;

			movementInfo.currSpeed += _gravity * Time.deltaTime;
			chipUITrans.position += Vector3.down * movementInfo.currSpeed * Time.deltaTime;
			if (chipUITrans.position.y <= targetSlot.transform.position.y) {
				var bottom = targetSlot.Surrounding4[(int) SlotSides.Bottom];
				if (bottom == null || bottom.CurrentChip != null) {
					targetSlot.SetChip (movementInfo.chipUI);
					movementInfo.Reset ();
				} else {
					movementInfo.targetSlot = bottom;
				}
			}
		}

		private class ChipMovementInfo {
			public M3ChipUI chipUI;
			public float currSpeed;
			public M3MapSlotUI targetSlot;

			public ChipMovementInfo (M3ChipUI chipUI) {
				this.chipUI = chipUI;
				currSpeed = 0;
				targetSlot = null;
			}

			public void Reset () {
				currSpeed = 0;
				targetSlot = null;
			}
		}
	}

}