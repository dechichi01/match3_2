﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Match3 {
	public class M3MapChipFiller {

		public M3MapChipFiller (M3ChipGetter chipGetter, System.Random prg) {
			_chipGetter = chipGetter;
			_prg = prg;
		}

		private M3ChipGetter _chipGetter;
		private System.Random _prg;
		public void FillWithPairs (Array2D<M3MapSlot> slots, int maxPairsCount) {
			int[] shuffledIndexes = Randomness.ShuffleArray (ArrayUtils.CreateRange (0, slots.Size), _prg);

			HashSet<int> ignore = new HashSet<int> ();
			M3ChipData[] randomChips = _chipGetter.GetRandomChips (maxPairsCount, 2);
			int pairsCount = 0;

			foreach (var index in shuffledIndexes) {
				if (pairsCount >= maxPairsCount) break;
				if (ignore.Contains (index)) continue;

				M3ChipData chip = randomChips[pairsCount];

				M3MapSlot slot = slots[index];
				SlotSides pairSide = GetRandomNeighbour (slot);
				M3MapSlot neighbour = slot.Surrounding4[(int) pairSide];

				slot.SetChip (chip);
				neighbour.SetChip (chip);

				var toIgnore = GetIgnorePiecesFromPair (slot, pairSide).Select (s => s.Index);
				ignore.UnionWith (toIgnore);

				pairsCount++;
			}
		}

		public void RandomFillEmptySlots (Array2D<M3MapSlot> slots) {
			slots.ForEach2D ((i, j, s) => {
				if (s.CurrentChip == null) {
					s.SetChip (_chipGetter.GetRandomChip (s.Surrounding4));
				}
			});
		}

		public void RemoveSolutions (List<M3MapSlot[]> startingSolutions) {
			foreach (M3MapSlot[] solution in startingSolutions) {
				Randomness.ShuffleArray (solution, _prg);
				for (int i = 0; i < solution.Length - 2; i++) {
					var slot = solution[i];
					slot.SetChip (_chipGetter.GetRandomChip (slot.Surrounding4));
				}
			}
		}

		//Knowing the side makes it easier to get the pair surroundings afterwards
		private SlotSides GetRandomNeighbour (M3MapSlot slot) {
			List<SlotSides> possibleSides = new List<SlotSides> (4);
			var surroundings = slot.Surrounding4;
			for (int i = 0; i < 4; i++) {
				if (surroundings[i] != null) possibleSides.Add ((SlotSides) i);
			}

			return possibleSides[Random.Range (0, possibleSides.Count)];
		}

		private IEnumerable<M3MapSlot> GetIgnorePiecesFromPair (M3MapSlot slot, SlotSides pairSide) {
			var slotSurroundings = slot.Surrounding4;
			var neighbour = slot.Surrounding4[(int) pairSide];
			var neighbourSurroundings = neighbour.Surrounding4;
			List<M3MapSlot> toIgnore = new List<M3MapSlot> (2 + 4) { slot, neighbour };

			var oppositeSide = pairSide.GetOppositSide ();
			var perpendiculars = pairSide.GetPerpendiculars ();

			toIgnore.AddRange (new [] {
				slotSurroundings[(int) oppositeSide], slotSurroundings[(int) perpendiculars[0]], slotSurroundings[(int) perpendiculars[1]],
					neighbourSurroundings[(int) pairSide], neighbourSurroundings[(int) perpendiculars[0]], neighbourSurroundings[(int) perpendiculars[1]],
			});

			return toIgnore.Where (s => s != null);
		}
	}
}