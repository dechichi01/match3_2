﻿namespace Match3 {
    public class M3MapSlot {
        private int _index;
        private int _i;
        private int _j;
        // private int _mapColumnCount;

        public int Index { get { return _index; } }

        public int I { get { return _i; } }
        public int J { get { return _j; } }

        private M3ChipData _currentChip;
        public M3ChipData CurrentChip { get { return _currentChip; } }

        public M3MapSlot[] Surrounding4 { get; private set; }
        public M3MapSlot[] Surrounding8 { get; private set; }

        public M3MapSlot (int i, int j, int index) {
            _index = index;
            _i = i;
            _j = j;
        }

        public void SetSurroundingSlots (Array2D<M3MapSlot> allSlots) {
            Surrounding8 = allSlots.GetSurroundings (_i, _j);
            Surrounding4 = new M3MapSlot[4];
            System.Array.Copy (Surrounding8, Surrounding4, 4);
        }

        public M3MapSlot GetSlot (SlotSides side) {
            return Surrounding8[(int) side];
        }

        public M3ChipData SetChip (M3ChipData chip) {
            var previous = _currentChip;
            _currentChip = chip;
            return previous;
        }
    }

    //If you alter this enum the world will explode.
    public enum SlotSides { Left = 0, Top = 1, Right = 2, Bottom = 3, BottomLeft = 4, TopLeft = 5, TopRight = 6, BottomRight = 7 }
}