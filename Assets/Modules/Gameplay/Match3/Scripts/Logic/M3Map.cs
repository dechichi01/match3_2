﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Random = UnityEngine.Random;

namespace Match3 {
    public class M3Map {
        private const int minPairs = 6;
        private const int maxPairs = 7;

        private Array2D<M3MapSlot> _slots;
        public Array2D<M3MapSlot> Slots { get { return _slots; } }

        private M3MapSolutionFinder _solutionChecker;

        private M3MapChipFiller _chipFiller;

        public M3Map (M3ChipGetter chipGetter, System.Random prg) {
            _chipFiller = new M3MapChipFiller (chipGetter, prg);
            _solutionChecker = new M3MapSolutionFinder ();
        }

        public Array2D<M3MapSlot> GenerateNewMap (int lineCount, int columnCount) {
            _slots = new Array2D<M3MapSlot> (lineCount, columnCount);

            _slots.ForEachAssign2D ((i, j, s) => new M3MapSlot (i, j, _slots.IndexFromIJ (i, j)));

            _slots.ForEach2D ((i, j, s) => {
                s.SetSurroundingSlots (_slots);
            });

            int pairsCount = UnityEngine.Random.Range (minPairs, maxPairs + 1);

            _chipFiller.FillWithPairs (_slots, pairsCount);
            _chipFiller.RandomFillEmptySlots (_slots);

            var startingSolutions = _solutionChecker.FindAllSolutions (_slots);
            _chipFiller.RemoveSolutions (startingSolutions);

            return _slots;
        }

        #region MapSolutionFinding
        public M3MapSlot[] FindSolutionForSlot (M3MapSlot slot) {
            return _solutionChecker.FindSolutionForSlot (slot, _slots.MaxDimension);
        }

        public M3MapSlot[][] SwapSlotsAndFindSolutions (M3MapSlot a, M3MapSlot b) {
            return _solutionChecker.SwapSlotsAndFindSolutions (this, a, b);
        }
        #endregion

        #region MapChipFill
        #endregion

        public void SwapChipInSlots (M3MapSlot a, M3MapSlot b) {
            var chip = a.SetChip (b.CurrentChip);
            b.SetChip (chip);
        }
    }
}