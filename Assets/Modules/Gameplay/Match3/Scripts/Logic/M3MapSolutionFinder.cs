﻿using System.Collections;
using System.Collections.Generic;

namespace Match3 {
	public class M3MapSolutionFinder {
		public M3MapSlot[][] SwapSlotsAndFindSolutions (M3Map map, M3MapSlot a, M3MapSlot b) {
			int maxDimension = map.Slots.MaxDimension;
			map.SwapChipInSlots (a, b);
			var solutionA = FindSolutionForSlot (a, maxDimension);
			var solutionB = FindSolutionForSlot (b, maxDimension);
			map.SwapChipInSlots (a, b);

			if (solutionA == null && solutionB == null) {
				return null;
			}

			if (solutionA != null && solutionB != null) {
				return new M3MapSlot[][] { solutionA, solutionB };
			}

			return new M3MapSlot[][] { solutionA ?? solutionB };
		}

		public M3MapSlot[] FindSolutionForSlot (M3MapSlot slot, int maxDimension) {
			return M3FloodFill.GetMatchFromNode (slot, maxDimension);
		}

		public List<M3MapSlot[]> FindAllSolutions (Array2D<M3MapSlot> slots) {
			List<M3MapSlot[]> solutions = new List<M3MapSlot[]> ();
			HashSet<M3MapSlot> solvedGroup = new HashSet<M3MapSlot> ();

			int maxDimension = slots.MaxDimension;

			slots.ForEach2D ((i, j, s) => {
				if (solvedGroup.Contains (s)) return;

				var solution = FindSolutionForSlot (s, maxDimension);
				if (solution != null && solution.Length >= M3FloodFill.MIN_CHAIN_FOR_MATCH) {
					solutions.Add (solution);
					solvedGroup.UnionWith (solution);
				}
			});

			return solutions;
		}
	}
}