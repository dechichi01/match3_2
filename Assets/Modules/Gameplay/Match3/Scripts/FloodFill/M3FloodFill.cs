﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MoreLinq;
using UnityEngine;

namespace Match3 {
	public static class M3FloodFill {
		public const int MIN_CHAIN_FOR_MATCH = 3; //Match 3...
		private static SlotSides[] Horizontal = new SlotSides[] { SlotSides.Left, SlotSides.Right };
		private static SlotSides[] Vertical = new SlotSides[] { SlotSides.Top, SlotSides.Bottom };
		public static M3MapSlot[] GetMatchFromNode (M3MapSlot node, int maxSingleDirectionMatches) {
			var targetChip = node.CurrentChip;
			if (targetChip == null) {
				return null;
			}

			HashSet<M3MapSlot> checkedSlots = new HashSet<M3MapSlot> ();

			var horizontalChain = FindSolutionInDirection (node, targetChip, Horizontal, checkedSlots, maxSingleDirectionMatches);

			var verticalSolutions = new List<List<M3MapSlot>> ();
			List<M3MapSlot> verticalContainingNode = null;
			foreach (var n in horizontalChain) {
				var verticalChain = FindSolutionInDirection (n, targetChip, Vertical, checkedSlots, maxSingleDirectionMatches);
				if (verticalChain.Count >= MIN_CHAIN_FOR_MATCH) {
					if (n == node) {
						verticalContainingNode = verticalChain;
					}
					verticalSolutions.Add (verticalChain);
				}
			}

			if (horizontalChain.Count >= MIN_CHAIN_FOR_MATCH && verticalSolutions.Count == 0) { // 1 horizontal solution
				return horizontalChain.ToArray ();
			}

			if (horizontalChain.Count < MIN_CHAIN_FOR_MATCH && verticalContainingNode != null) { // 1 vertical solution
				var verticalSolution = verticalContainingNode.ToArray ();
				return verticalSolution;
			}

			if (horizontalChain.Count >= MIN_CHAIN_FOR_MATCH && verticalSolutions.Count > 0) { // 1 horizontal and 1 vertical solution, we can safely merge
				HashSet<M3MapSlot> solution = new HashSet<M3MapSlot> (horizontalChain);
				solution.UnionWith (verticalSolutions.MaxBy (s => s.Count));

				return solution.ToArray ();
			}

			return null;
		}
		private static List<M3MapSlot> FindSolutionInDirection (M3MapSlot n, M3ChipData targetChip, SlotSides[] direction,
			HashSet<M3MapSlot> checkedSlots, int maxSingleDirectionMatches) {

			List<M3MapSlot> dirSolution = new List<M3MapSlot> (maxSingleDirectionMatches);
			dirSolution.Add (n);
			checkedSlots.Add (n);

			var neighbours = n.Surrounding4;

			foreach (var side in direction) {
				var target = neighbours[(int) side];
				while (target != null && target.CurrentChip == targetChip) {
					dirSolution.Add (target);
					target = target.Surrounding4[(int) side];
				}
			}

			checkedSlots.UnionWith (dirSolution);

			return dirSolution;
		}
	}

}