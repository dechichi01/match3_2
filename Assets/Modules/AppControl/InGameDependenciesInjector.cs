﻿using System.Collections;
using System.Collections.Generic;
using Match3;
using UnityEngine;

public class InGameDependenciesInjector : MonoBehaviour {
	[SerializeField] private M3MapDelegator _mapDelegator;
	[SerializeField] private M3MapUI _mapUI;

	[Header ("Dependents")]
	[SerializeField] private GameObject[] _mapDelegatorDependents;
	[SerializeField] private GameObject[] _mapUIDependents;

	public void InjectDependencies () {
		foreach (var go in _mapDelegatorDependents) {
			var dependents = go.GetComponents<IM3MapDelegatorDependent> ();
			foreach (var dependent in dependents) {
				dependent.MapDelegator = _mapDelegator;
			}
		}

		foreach (var go in _mapUIDependents) {
			var dependents = go.GetComponents<IM3MapUIDependent> ();
			foreach (var dependent in dependents) {
				dependent.MapUI = _mapUI;
			}
		}
	}
}