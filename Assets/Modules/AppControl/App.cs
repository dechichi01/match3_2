﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class App : MonoBehaviour {

	[SerializeField] private InGameDependenciesInjector _inGameDepsInjector;

	private void Awake () {
		_inGameDepsInjector.InjectDependencies ();
	}
}