﻿using System.Collections;
using System.Collections.Generic;
using Lean;
using UnityEngine;

public class MobileStaticJoyscitk : MonoBehaviour {

    [SerializeField] private StaticJoystickInputArea _inputArea;
    [SerializeField] private RectTransform _inputIndicator;

    private Vector2 _inputValue;
    private Vector2 _jumpInput;

    private LeanFinger _currFinger;

    public Vector2 InputValue {
        get {
            return _inputValue;
        }
    }

    private void OnEnable () {
        _inputIndicator.gameObject.SetActive (false);
        LeanTouch.OnFingerDown += OnFingerDown;
        LeanTouch.OnFingerUp += OnFingerUp;
        _currFinger = null;
        _inputValue = Vector2.zero;
    }

    private void OnDisable () {
        _inputIndicator.gameObject.SetActive (false);
        LeanTouch.OnFingerDown -= OnFingerDown;
        LeanTouch.OnFingerUp -= OnFingerUp;
    }

    public void Update () {
        if (_currFinger != null) {
            _inputValue = _inputArea.GetMovementInput (_currFinger.ScreenPosition);
            float angle = Vector2.Angle (_inputValue, Vector2.right) * Mathf.Sign (_inputValue.y);
            _inputIndicator.rotation = Quaternion.AngleAxis (angle, Vector3.forward);
        }
    }

    private void OnFingerDown (LeanFinger finger) {
        if (_currFinger == null && _inputArea.ContainsScreenPoint (finger.ScreenPosition)) {
            _currFinger = finger;
            _inputIndicator.gameObject.SetActive (true);
        }
    }

    private void OnFingerUp (LeanFinger finger) {
        if (_currFinger == finger) {
            _currFinger = null;
            _inputIndicator.gameObject.SetActive (false);
            _inputValue = Vector2.zero;
        }
    }

    private void OnDrawGizmosSelected () {
        if (_inputArea.Area == null)
            return;

        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere (_inputArea.Area.position, _inputArea.InputRadius);
    }
}

[System.Serializable]
public class StaticJoystickInputArea : InputRectArea {
    [SerializeField] private Canvas _canvas;

    public float InputRadius {
        get {
            if (_canvas == null) return 0;
            Vector2 size = area.rect.size * _canvas.scaleFactor;
            return Mathf.Min (size.x, size.y) / 2;
        }
    }

    public Vector2 GetMovementInput (Vector2 reference) {
        return (reference - (Vector2) area.position).normalized;
    }

    public override bool ContainsScreenPoint (Vector2 point) {
        return base.ContainsScreenPoint (point) &&
            (point - (Vector2) area.position).sqrMagnitude < InputRadius * InputRadius;
    }
}