﻿using Lean;
using UnityEngine;

public class FingerDragDetector : MonoBehaviour {

    [SerializeField] private InputRectArea dragArea;

    private LeanFinger dragFinger;
    private float dragFingerDownTime;

    public System.Action<Vector2> OnFingerDown;
    public System.Action<Vector2, Vector2> OnFingerDrag;
    public System.Action<Vector2> OnFingerUp;

    private void OnEnable () {
        LeanTouch.OnFingerDown += FingerDown;
        LeanTouch.OnFingerDrag += FingerDrag;
        LeanTouch.OnFingerUp += FingerUp;
    }

    private void OnDisable () {
        LeanTouch.OnFingerDown -= FingerDown;
        LeanTouch.OnFingerDrag -= FingerDrag;
        LeanTouch.OnFingerUp -= FingerUp;
    }

    private void FingerDown (LeanFinger finger) {
        if (dragFinger == null && dragArea.ContainsScreenPoint (finger.ScreenPosition)) {
            dragFinger = finger;
            dragFingerDownTime = Time.time;

            if (OnFingerDown != null) {
                OnFingerDown (dragFinger.ScreenPosition);
            }
        }
    }

    private void FingerUp (LeanFinger finger) {
        if (finger == dragFinger) {
            dragFinger = null;
            if (OnFingerUp != null) {
                OnFingerUp (finger.ScreenPosition);
            }
        }
    }

    private void FingerDrag (LeanFinger finger) {
        if (finger == dragFinger) {
            if (OnFingerDrag != null) {
                OnFingerDrag (finger.ScreenPosition, finger.DeltaScreenPosition);
            }
        }
    }
}