﻿using UnityEngine;
using System.Collections;
using System.Reflection;

public class Singleton<T> : MonoBehaviour where T : Singleton<T>, new()
{
    protected static T _instance;

    public static T instance
    {
        get
        {
            if (_instance == null)
                _instance = new T();

            return _instance;
        }
    }

    public Singleton()
    {

    }

}
