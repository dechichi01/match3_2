﻿using UnityEngine;
using System.Collections;
using System.Reflection;

/// <summary>
/// Singleton pattern for Monobehaviour. As for now haven't found a suitable way to make it thread safe.
/// </summary>
/// <typeparam name="T">Subclass type. Ex: SubClass : SingletonMono<SubClass></typeparam>
public abstract class SingletonMonoNoCreate<T> : MonoBehaviour where T : SingletonMonoNoCreate<T>, new()
{
    protected static T _instance;
    
    public static T instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<T>();
            if (_instance == null)
                throw new System.Exception("You need to create a instance of " + typeof(T).Name + " before referencing it");

            return _instance;
        }
    }

    protected virtual void Awake()
    {
        if (_instance == null)
        {
            _instance = (T)this;
            DontDestroyOnLoad(transform.root.gameObject);
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
            return;
        }
    }
}
